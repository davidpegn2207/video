using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class maus : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    public GameObject popupWindowObject;

    private void Start()
    {
        popupWindowObject.SetActive(false);
    }

    private void Update()
    {
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
       
        popupWindowObject.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
      
        popupWindowObject.SetActive(false);
    }
}
