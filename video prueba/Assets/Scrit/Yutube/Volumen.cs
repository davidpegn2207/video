using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class Volumen : MonoBehaviour
{
    public Slider slider;
    public float Audio;
    public GameObject RawImagen;

    // Start is called before the first frame update
    void Start()
    {
        Audio = slider.value;
    }

    // Update is called once per frame
    void Update()
    {
        //Sonido();
    }
     public void Sonido()
     {
        RawImagen.GetComponent<VideoPlayer>().SetDirectAudioVolume(0, Audio);
     }
   
}
